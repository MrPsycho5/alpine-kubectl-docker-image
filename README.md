This image is based on `alpine:3.16`. Multiple kubectl versions are available as tags. The default version is `v1.26.1`.

The default ENTRYPOINT of this image is the default entrypoint of the `alpine:3.16` image. Use `kubectl` as the argument to run the `kubectl` command.

IF you are missing a version, please open an issue.